import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class TopBowlersTest {
    TopBowlers ipl = new TopBowlers();
    ArrayList actual = new ArrayList();
    @Test
    @DisplayName("top 10 ecomonical Bowlers")
    public void top10EconomicalBowlers() throws Exception {
        String[] expected = {"RN ten Doeschate=2.0", "V Kohli=2.5", "J Yadav=3.625", "Parvez Rasool=5.1666665", "CH Gayle=5.3333335", "S Nadeem=5.375", "M Vijay=5.6", "DR Smith=5.6666665", "R Ashwin=5.725", "M de Lange=6.0"};
        ArrayList actual = ipl.top10EconomicalBowlers();
        assertArrayEquals(expected, actual.toArray(), "arrays un-matched");
        System.out.println("top 10 ecomonical Bowlers");
    }
    @Test
    @DisplayName("length of array")
    public void checkLength() throws Exception {
        actual = ipl.top10EconomicalBowlers();
        assertEquals(10, actual.size());
    }

    @Test
    @DisplayName("Non-empty")
    public void checkEmpty() throws Exception {
        actual = ipl.top10EconomicalBowlers();
        assertFalse(actual.isEmpty());
    }

}