import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class BowlingFiguresTest {
    ArrayList actualTeam1 = new ArrayList();
    ArrayList actualTeam2 = new ArrayList();

    @Test
    @DisplayName("Both teams bowling figures")
    public void bowlingFiguresPerId() throws Exception {
        BowlingFigures ipl = new BowlingFigures();
        String[] expectedTeam1 = {"id: 1=Royal Challengers Bangalore", "Total balls=124", "Total overs=20", "Wide runs=5", "Extra runs=7", "TS Mills=32", "Super over=0", "A Choudhary=55", "YS Chahal=22", "S Aravind=36", "SR Watson=41", "TM Head=11", "STR Binny=10"};
        String[] expectedTeam2 = {"id: 1=Sunrisers Hyderabad", "Total balls=123", "Total overs=20", "Wide runs=4", "Extra runs=6", "A Nehra=42", "Super over=0", "B Kumar=28", "BCJ Cutting=35", "Rashid Khan=36", "DJ Hooda=7", "MC Henriques=20", "Bipul Sharma=4"};
        ArrayList actualTeam1 = ipl.bowlingFigureByMatchIdTeam1();
        ArrayList actualTeam2 = ipl.bowlingFigureByMatchIdTeam2();
        assertArrayEquals(expectedTeam1, actualTeam1.toArray());
        assertArrayEquals(expectedTeam2, actualTeam2.toArray());
        System.out.println("Both teams bowling figures");
    }

    @Test
    @DisplayName("length of array")
    public void checkLength() throws Exception {
        BowlingFigures ipl = new BowlingFigures();
        actualTeam1 = ipl.bowlingFigureByMatchIdTeam1();
        actualTeam2 = ipl.bowlingFigureByMatchIdTeam1();

        assertEquals(13, actualTeam1.size());
        assertEquals(13, actualTeam2.size());
    }

    @Test
    @DisplayName("Non-empty")
    public void checkEmpty() throws Exception {
        BowlingFigures ipl = new BowlingFigures();
        actualTeam1 = ipl.bowlingFigureByMatchIdTeam1();
        actualTeam2 = ipl.bowlingFigureByMatchIdTeam2();
        assertFalse(actualTeam1.isEmpty());
        assertFalse(actualTeam2.isEmpty());
    }

}