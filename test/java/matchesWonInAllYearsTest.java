import org.junit.Test;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class matchesWonInAllYearsTest {
    ArrayList actual = new ArrayList();

    @Test
    @DisplayName("Won matches by each team per year")
    public void matchesWonPerTeam() throws Exception {
        matchesWonInAllYears ipl = new matchesWonInAllYears();
        String[] expected = {"Chennai Super Kings=79", "Deccan Chargers=29", "Delhi Daredevils=62", "Gujarat Lions=13", "Kings XI Punjab=70", "Kochi Tuskers Kerala=6", "Kolkata Knight Riders=77", "Mumbai Indians=92", "Pune Warriors=12", "Rajasthan Royals=63", "Rising Pune Supergiant=10", "Rising Pune Supergiants=5", "Royal Challengers Bangalore=73", "Sunrisers Hyderabad=42"};
        ArrayList actual = ipl.matchesWonPerTeamInAllYears();
        assertArrayEquals(expected, actual.toArray(), "arrays not matched");
        System.out.println("Won matches by each team per year");
    }
    @Test
    @DisplayName("length of expected array")
    public void checkLength() throws Exception {
        matchesWonInAllYears ipl = new matchesWonInAllYears();
        actual = ipl.matchesWonPerTeamInAllYears();
        assertEquals(14, actual.size(),"size should be 14");
    }

    @Test
    @DisplayName("Non-empty")
    public void checkEmpty() throws Exception {
        matchesWonInAllYears ipl = new matchesWonInAllYears();
        actual = ipl.matchesWonPerTeamInAllYears();
        assertFalse(actual.isEmpty(),"should not empty");
    }

}
