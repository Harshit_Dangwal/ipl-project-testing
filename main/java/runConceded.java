//  For the year 2016 get the extra runs conceded per team.

import java.io.*;
import java.util.*;

class runConceded{
    public ArrayList runConcededPerTeamIn2016() throws Exception {
        BufferedReader matchDetails = new BufferedReader(new FileReader("/home/harshit/IdeaProjects/IPLProjectTesting/src/main/java/matches.csv"));
        BufferedReader deliveryDetails = new BufferedReader(new FileReader("/home/harshit/IdeaProjects/IPLProjectTesting/src/main/java/deliveries.csv"));
        String matchLine = "";
        String deliveryLine = "";
        SortedMap<String, Integer> result = new TreeMap<>();
        ArrayList<String> idList = new ArrayList<>();
        while ((matchLine = matchDetails.readLine()) != null) {
            String[] matchRecord = matchLine.split(",");
            if (matchRecord[1].contains("2016")) {
                idList.add(matchRecord[0]);
            }
        }
        while ((deliveryLine = deliveryDetails.readLine()) != null) {
            String[] deliveryRecord = deliveryLine.split(",");
            if (idList.contains(deliveryRecord[0])) {
                if (result.containsKey(deliveryRecord[3])) {
                    result.put(deliveryRecord[3], result.get(deliveryRecord[3]) + Integer.parseInt(deliveryRecord[16]));
                } else {
                    result.put(deliveryRecord[3], Integer.parseInt(deliveryRecord[16]));
                }
            }
        }
        ArrayList<String> finalResult= new ArrayList<String>();
        for(String key:result.keySet()){
            finalResult.add(key+"="+result.get(key));
        }
        return finalResult;
    }
}
