//Number of matches won of all teams over all the years of IPL.

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.SortedMap;
import java.util.TreeMap;

class matchesWonInAllYears {
    ArrayList matchesWonPerTeamInAllYears() throws Exception {
        BufferedReader matchData = new BufferedReader(new FileReader("/home/harshit/IdeaProjects/IPLProjectTesting/src/main/java/matches.csv"));
        SortedMap<String, Integer> result = new TreeMap<>();
        String line = "";
        Integer temp;
        while ((line = matchData.readLine()) != null) {
            String[] record = line.split(",");
            if(record[10].length()==0){
                continue;
            }
            else if(record[0].contains("id")) {
                continue;
            } else if (result.containsKey(record[10])) {
                result.put(record[10], result.get(record[10]) + 1);
            }  else {
                temp = 1;
                result.put(record[10], temp);
            }
        }
        ArrayList finalResult = new ArrayList();
        for(String key:result.keySet()){
            finalResult.add(key+"="+result.get(key));
        }
        return finalResult;
    }
}
