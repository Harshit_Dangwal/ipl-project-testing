import org.junit.Test;
import org.junit.jupiter.api.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class matchesPlayedPerYearTest {
    matchesPlayedPerYear ipl = new matchesPlayedPerYear();
    String[] expected = {"2008=58", "2009=57", "2010=60", "2011=73", "2012=74", "2013=76", "2014=60", "2015=59", "2016=60", "2017=59"};
    ArrayList actual = new ArrayList();

    @Test
    @DisplayName("matches played per year")
    public void matchesPlayedPerYear() throws Exception {
        actual = ipl.matchesPlayedPerYearInAllYears();
        assertArrayEquals(expected, actual.toArray(), "arrays not matched");
        System.out.println("matches played per year");
    }

    @Test
    @DisplayName("length of array")
    public void checkLength() throws Exception {
        actual = ipl.matchesPlayedPerYearInAllYears();
        assertEquals(10, actual.size());
    }

    @Test
    @DisplayName("Non-empty")
    public void checkEmpty() throws Exception {
        actual = ipl.matchesPlayedPerYearInAllYears();
        assertFalse(actual.isEmpty());
    }
}
