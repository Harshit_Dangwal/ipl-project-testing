import org.junit.runner.*;
import org.junit.runner.notification.Failure;

public class Main {
        public static void main(String[] args) {
            Result result = JUnitCore.runClasses(JunitTestSuite.class);

            for (Failure failure : result.getFailures()) {
                System.out.println(failure.toString());
            }

            System.out.println(result.wasSuccessful());
        }
    }

