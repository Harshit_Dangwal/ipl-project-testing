import org.junit.Test;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class runConcededIn2016Test{
    ArrayList actual = new ArrayList();
    runConceded ipl =new runConceded();
    @Test
    @DisplayName("Run conceded in 2016")
    public void runConcededByEachTeam() throws Exception{
        String [] expected={"Delhi Daredevils=106", "Gujarat Lions=98", "Kings XI Punjab=100", "Kolkata Knight Riders=122", "Mumbai Indians=102", "Rising Pune Supergiants=108", "Royal Challengers Bangalore=156", "Sunrisers Hyderabad=107"};
        ArrayList actual = ipl.runConcededPerTeamIn2016();
        assertArrayEquals(expected,actual.toArray(),"arrays not matched");
        System.out.println("Run conceded in 2016");
    }
    @Test
    @DisplayName("length of array")
    public void checkLength() throws Exception {
        actual = ipl.runConcededPerTeamIn2016();
        assertEquals(8, actual.size());
    }

    @Test
    @DisplayName("Non-empty")
    public void checkEmpty() throws Exception {
        actual = ipl.runConcededPerTeamIn2016();
        assertFalse(actual.isEmpty());
    }
    @Test
    @DisplayName("not null checked")
    public void forNotNull() throws Exception{
        assertNotNull(ipl.runConcededPerTeamIn2016());
    }

}