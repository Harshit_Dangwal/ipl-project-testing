//Display the bowling figures of each teams for a particular match(EX:id=1).

import java.util.*;
import java.io.*;

public class BowlingFigures {
    static String inputId = "1";

    //method team 1.
    ArrayList bowlingFigureByMatchIdTeam1() throws Exception {
        BufferedReader deliveryData = new BufferedReader(new FileReader("/home/harshit/IdeaProjects/IPLProjectTesting/src/main/java/deliveries.csv"));
        BufferedReader matchesData = new BufferedReader(new FileReader("/home/harshit/IdeaProjects/IPLProjectTesting/src/main/java/matches.csv"));
        LinkedHashMap result1 = new LinkedHashMap();
        String matchLine = "";
        String deliveryLine = "";
        String team1 = "";
        // saving given matchId(inputId) in team1 and team2.
        while ((deliveryLine = deliveryData.readLine()) != null) {
            String[] deliveryRecord = deliveryLine.split(",");
            if (deliveryRecord[0].contains(inputId)) {
                team1 = deliveryRecord[3];
                break;
            }
        }
        // iterating through deliveries.csv file.
        while ((deliveryLine = deliveryData.readLine()) != null) {
            String temp = "1";
            String[] deliveryRecord = deliveryLine.split(",");
            if (inputId.contains(deliveryRecord[0])) {
                if (deliveryRecord[3].contains(team1)) {
                    if (result1.containsKey("id: " + inputId)) {
                        result1.put("Total balls", Integer.parseInt(result1.get("Total balls").toString()) + 1);
                        if (result1.containsKey(deliveryRecord[8])) {
                            result1.put(deliveryRecord[8], Integer.parseInt(result1.get(deliveryRecord[8]).toString()) + Integer.parseInt(deliveryRecord[17]));
                        } else {
                            result1.put(deliveryRecord[8], Integer.parseInt(deliveryRecord[17]));
                        }
                        result1.put("Super over", Integer.parseInt(result1.get("Super over").toString()) + Integer.parseInt(deliveryRecord[9]));
                        if (deliveryRecord[5].contains("1")) {
                            result1.put("Total overs", Integer.parseInt(result1.get("Total overs").toString()) + 1);
                        }
                        if (result1.containsKey("Extra runs")) {
                            result1.put("Extra runs", Integer.parseInt(result1.get("Extra runs").toString()) + Integer.parseInt(deliveryRecord[16]));
                        } else {
                            result1.put("Extra runs", Integer.parseInt(deliveryRecord[16]));
                        }
                        if (result1.containsKey("Wide runs")) {
                            result1.put("Wide runs", Integer.parseInt(result1.get("Wide runs").toString()) + Integer.parseInt(deliveryRecord[10]));
                        } else {
                            result1.put("Wide runs", Integer.parseInt(deliveryRecord[10]));
                        }
                    } else {
                        // result1.put(deliveryRecord[3],0);
                        result1.put("id: " + inputId, deliveryRecord[3]);
                        result1.put("Total balls", 1);
                        result1.put("Total overs", 1);
                        result1.put("Wide runs", Integer.parseInt(deliveryRecord[10]));
                        result1.put("Extra runs", Integer.parseInt(deliveryRecord[16]));
                        result1.put(deliveryRecord[8], Integer.parseInt(deliveryRecord[17]));
                        result1.put("Super over", Integer.parseInt(deliveryRecord[9]));

                    }
                }
            }
        }
        ArrayList finalResult1 =new ArrayList();
        for(Object key:result1.keySet()){
            finalResult1.add(key+"="+result1.get(key));
        }
        return finalResult1;
    }

    // method for team 2.
    ArrayList bowlingFigureByMatchIdTeam2() throws Exception {
        BufferedReader deliveryData = new BufferedReader(new FileReader("/home/harshit/IdeaProjects/IPLProjectTesting/src/main/java/deliveries.csv"));
        BufferedReader matchesData = new BufferedReader(new FileReader("/home/harshit/IdeaProjects/IPLProjectTesting/src/main/java/matches.csv"));
        LinkedHashMap result2 = new LinkedHashMap();
        String deliveryLine = "";
        String team2 = "";


        // saving given matchId(inputId) in team1 and team2.
        while ((deliveryLine = deliveryData.readLine()) != null) {
            String[] deliveryRecord = deliveryLine.split(",");
            if (deliveryRecord[0].contains(inputId)) {
                team2 = deliveryRecord[2];
                break;
            }
        }

        // iterating through deliveries.csv file.
        while ((deliveryLine = deliveryData.readLine()) != null) {
            String[] deliveryRecord = deliveryLine.split(",");
            if (inputId.contains(deliveryRecord[0])) {
                if (deliveryRecord[3].contains(team2)) {
                    if (result2.containsKey("id: " + inputId)) {
                        result2.put("Total balls", Integer.parseInt(result2.get("Total balls").toString()) + 1);
                        if (result2.containsKey(deliveryRecord[8])) {
                            result2.put(deliveryRecord[8], Integer.parseInt(result2.get(deliveryRecord[8]).toString()) + Integer.parseInt(deliveryRecord[17]));
                        } else {
                            result2.put(deliveryRecord[8], Integer.parseInt(deliveryRecord[17]));
                        }
                        result2.put("Super over", Integer.parseInt(result2.get("Super over").toString()) + Integer.parseInt(deliveryRecord[9]));
                        if (deliveryRecord[5].contains("1")) {
                            result2.put("Total overs", Integer.parseInt(result2.get("Total overs").toString()) + 1);
                        }
                        if (result2.containsKey("Extra runs")) {
                            result2.put("Extra runs", Integer.parseInt(result2.get("Extra runs").toString()) + Integer.parseInt(deliveryRecord[16]));
                        } else {
                            result2.put("Extra runs", Integer.parseInt(deliveryRecord[16]));
                        }
                        if (result2.containsKey("Wide runs")) {
                            result2.put("Wide runs", Integer.parseInt(result2.get("Wide runs").toString()) + Integer.parseInt(deliveryRecord[10]));
                        } else {
                            result2.put("Wide runs", Integer.parseInt(deliveryRecord[10]));
                        }
                    } else {
                        // result1.put(deliveryRecord[3],0);
                        result2.put("id: " + inputId, deliveryRecord[3]);
                        result2.put("Total balls", 1);
                        result2.put("Total overs", 1);
                        result2.put("Wide runs", Integer.parseInt(deliveryRecord[10]));
                        result2.put("Extra runs", Integer.parseInt(deliveryRecord[16]));
                        result2.put(deliveryRecord[8], Integer.parseInt(deliveryRecord[17]));
                        result2.put("Super over", Integer.parseInt(deliveryRecord[9]));

                    }
                }
            }
        }
        ArrayList finalResult2 =new ArrayList();
        for(Object key:result2.keySet()){
            finalResult2.add(key+"="+result2.get(key));
        }
        return finalResult2;
    }
}

//public class bowlingFigures {
//    public static void main(String[] args) throws Exception {
//        cricket ipl = new cricket();
//        System.out.println(ipl.bowlingFigureByMatchIdTeam1());
//        System.out.println(ipl.bowlingFigureByMatchIdTeam2());
//        BufferedWriter file = new BufferedWriter(new FileWriter("bowlingFigures.txt", false));
//        file.write("bowlingFigures of team 1: \n" + ipl.bowlingFigureByMatchIdTeam1().toString() + "\n\n");
//        file.write("bowlingFigures of team 2: \n" + ipl.bowlingFigureByMatchIdTeam2().toString());
//        file.close();
//    }
//}
//
///*
//
//{id: 1=Royal Challengers Bangalore, Total balls=124, Total overs=20, Wide runs=5, Extra runs=7, TS Mills=32, Super over=0, A Choudhary=55, YS Chahal=22, S Aravind=36, SR Watson=41, TM Head=11, STR Binny=10}
//{id: 1=Sunrisers Hyderabad, Total balls=123, Total overs=20, Wide runs=4, Extra runs=6, A Nehra=42, Super over=0, B Kumar=28, BCJ Cutting=35, Rashid Khan=36, DJ Hooda=7, MC Henriques=20, Bipul Sharma=4}
//
// */