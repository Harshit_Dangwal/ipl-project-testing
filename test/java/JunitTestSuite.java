import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)

@SuiteClasses({
        matchesPlayedPerYearTest.class,
        matchesWonInAllYearsTest.class,
        runConcededIn2016Test.class,
        TopBowlersTest.class,
        BowlingFiguresTest.class

})

public class JunitTestSuite {
    @BeforeClass
    public static void start() {
        System.out.println("Testing");
    }
}
