import java.util.*;
import java.io.*;

class TopBowlers {
    protected ArrayList top10EconomicalBowlers() throws Exception {
        BufferedReader matchDetails = new BufferedReader(new FileReader("/home/harshit/IdeaProjects/IPLProjectTesting/src/main/java/matches.csv"));
        BufferedReader deliveryDetails = new BufferedReader(new FileReader("/home/harshit/IdeaProjects/IPLProjectTesting/src/main/java/deliveries.csv"));
        SortedMap<String, Float> result = new TreeMap<String, Float>();
        SortedMap<String, Float> bowlersOver = new TreeMap<String, Float>();
        ArrayList<String> idList = new ArrayList<String>();
        String matchLine = "";
        String deliveryLine = "";
// list of ids of year 2015.
        while ((matchLine = matchDetails.readLine()) != null) {
            String[] matchRecord = matchLine.split(",");
            if (matchRecord[1].contains("2015")) {
                idList.add(matchRecord[0]);
            }
        }
        while ((deliveryLine = deliveryDetails.readLine()) != null) {
            String[] deliveryRecord = deliveryLine.split(",");
            if (idList.contains(deliveryRecord[0])) {
                if (result.containsKey(deliveryRecord[8])) {
                    result.put(deliveryRecord[8], result.get(deliveryRecord[8]) + Integer.parseInt(deliveryRecord[17]));
                } else {
                    result.put(deliveryRecord[8], Float.parseFloat(deliveryRecord[17]));
                }
            }
            if (idList.contains(deliveryRecord[0])) {
                if (deliveryRecord[5].contains("6")) {
                    bowlersOver.put(deliveryRecord[8], bowlersOver.get(deliveryRecord[8]) + 1);
                } else {
                    if (!bowlersOver.containsKey(deliveryRecord[8]))
                        bowlersOver.put(deliveryRecord[8], Float.parseFloat(deliveryRecord[5]));
                }
            }
        }
        for (String bowler : bowlersOver.keySet()) {
            if (result.containsKey(bowler)) {
                result.put(bowler, result.get(bowler) / bowlersOver.get(bowler));
            }
        }
        ArrayList<Float> sortedBowlerEco = new ArrayList<>();
        for (Float economy : result.values()) {
            sortedBowlerEco.add(economy);
        }
        Collections.sort(sortedBowlerEco);
        LinkedHashMap<String, Float> top10EconomicalBowlers = new LinkedHashMap<String, Float>();
        for (Float value : sortedBowlerEco) {
            for (String bowler : result.keySet()) {
                if (result.get(bowler) == value) {
                    top10EconomicalBowlers.put(bowler, value);
                }
            }
            if (top10EconomicalBowlers.size() == 10) {
                break;
            }
        }
        ArrayList<String> finalResult= new ArrayList<String>();
        for(String key:top10EconomicalBowlers.keySet()){
            finalResult.add(key+"="+top10EconomicalBowlers.get(key));
        }
        return finalResult;
    }
}
//public class economicalBowlers {
//    public static void main(String[] args) throws Exception {
//        Match ipl = new Match();
//        System.out.println("For the year 2015, the top 10 economical bowlers: \n" + ipl.top10EconomicalBowlers());
//        BufferedWriter file = new BufferedWriter(new FileWriter("./solution4.txt", false));
//        file.write("For the year 2015, the top 10 economical bowlers. \n" + ipl.top10EconomicalBowlers().toString());
//        file.close();
//    }
//}

/*

For the year 2015, the top 10 economical bowlers:
{RN ten Doeschate=2.0, V Kohli=2.5, J Yadav=3.625, Parvez Rasool=5.1666665, CH Gayle=5.3333335, S Nadeem=5.375, M Vijay=5.6, DR Smith=5.6666665, R Ashwin=5.725, M de Lange=6.0}

 */

